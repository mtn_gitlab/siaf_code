/**
 * View Models used by Spring MVC REST controllers.
 */
package com.orange.jh4.web.rest.vm;
